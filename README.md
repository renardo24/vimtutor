# vim tutor

`tutor-1.7`: file copied from `/usr/share/vim/vim80/tutor/tutor` on UBuntu 18.04.

Links:

* [How To Learn Vim: A Four Week Plan](https://medium.com/actualize-network/how-to-learn-vim-a-four-week-plan-cd8b376a9b85)
* [A Good Vimrc](https://dougblack.io/words/a-good-vimrc.html)
* [Vim Book](http://ftp.vim.org/pub/vim/doc/book/vimbook-OPL.pdf)
* [Vim graph lessons](http://www.viemu.com/a_vi_vim_graphical_cheat_sheet_tutorial.html)

